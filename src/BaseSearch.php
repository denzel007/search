<?php

namespace Tetrapak07\Search;

use Phalcon\Mvc\Controller;
use Supermodule\ControllerBase as SupermoduleBase;

class BaseSearch extends Controller
{
      
    public function initialize()
    {

    }
    
    public function onConstruct()
    {
      $this->dataReturn = true;
        if (!SupermoduleBase::checkAndConnectModule('search')) {
            $this->dataReturn = false;
        }   
    }
    
    public function search($search, $limit = 0)
    {
        if (!$this->dataReturn) {
            return false;
        }
        $searchOptions = $this->config->modules->search->searchModels->toArray();
        return $this->searchWithOptions($search, $limit, $searchOptions);

    }
    
    public function searchWithOptions($search, $limit = 0, $searchOptions = false)
    {
        $result = false;
        if (empty($searchOptions)OR (count($searchOptions) ==0)) {
            return false;
        }
        $sql = 'SELECT '; $sqlFrom = '';$sqlSelect = '';$selectColumns = '';
        $sqlWhere = '';
        foreach ($searchOptions as $model => $columns) {

            if (count($searchOptions)>1) {
            $sqlFrom = $model.', '.$sqlFrom;
            } else {
              $sqlFrom = $model;  
            }

            foreach ($columns as $column) {
                $modelExplode = explode('\\',$model);
                $selectColumns = $model.'.'.$column.' as '.strtolower(array_pop ($modelExplode )).'_'.$column.', '.$selectColumns;
                $sqlWhere = $model.'.'.$column.' like :search: or '.$sqlWhere;
            }
        }
        # $sqlFrom = substr($sqlFrom, 0, -2);
        $selectColumns = substr($selectColumns, 0, -2);
        $sqlWhere = substr($sqlWhere, 0, -3);

        $phql = $sql.$selectColumns.' FROM '.$sqlFrom. ' WHERE ('.$sqlWhere.') LIMIT '.(int) $limit.'';
        
        # echo '<br>sql: '.$phql.'<br><br>';

        try {
 
            $result = $this->modelsManager->executeQuery($phql,  [
                  "search" => '%' . $search . '%',
                  # "lim" => 1,        
            ])->toArray();
 
        } catch (PDOException $ex) {
            var_dump($ex);
        }
        
        return $result;
    }

    
}